# README #

This is a repo with T-Systems Java School preliminary examination tasks.

The exam includes 2 tasks: [Pyramid](/tasks/Pyramid.md), [Zones](/tasks/Zones.md)

### Result ###

* Author name : Nikita Sinyaev
* Codeship : [ ![Codeship Status for DarkRerum/javaschoolexam](https://app.codeship.com/projects/3b6a4480-783c-0135-7e24-0e938cc2a464/status?branch=master)](https://app.codeship.com/projects/244819)