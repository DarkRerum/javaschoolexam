package com.tsystems.javaschool.tasks.zones;

import java.util.ArrayList;
import java.util.List;

public class RouteChecker {

    private boolean[][] linkMatrix;

    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        for (Integer i : requestedZoneIds) {
            if (i == null) {
                return false;
            }
            if (i < 1 || i > zoneState.size()) {
                return false;
            }
        }
        linkMatrix = new boolean[zoneState.size()][zoneState.size()];

        for (int i = 0; i < requestedZoneIds.size(); i++) {
            for (int j = i; j < requestedZoneIds.size(); j++) {
                if (!checkLink(zoneState, requestedZoneIds.get(i), requestedZoneIds.get(j), 0, requestedZoneIds)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Checks whether two required zones are connected with each other through allowed zones.
     * @param zoneState current list of all available zones
     * @param startId start zone's ID
     * @param endId end node's ID
     * @param depth used to prevent stack overflow during recursive traversal
     * @param allowedIds list of zone IDs
     * @return true of zones are connected, false otherwise
     */
    private boolean checkLink(List<Zone> zoneState, int startId, int endId, int depth, List<Integer> allowedIds) {
        if (depth++ >= zoneState.size()) {
            return false;
        }
        if (startId == endId) {
            return true;
        }
        if (linkMatrix[startId - 1][endId - 1]) {
            return true;
        }

        List<Integer> neighbours = new ArrayList<>();
        neighbours.addAll(zoneState.get(startId - 1).getNeighbours());
        for (Integer i : allowedIds) {
            if (zoneState.get(i - 1).getNeighbours().contains(startId)) {
                if (!neighbours.contains(i)) {
                    neighbours.add(i);
                }
            }
        }

        for (Integer i : neighbours) {
            if (!allowedIds.contains(i)) {
                continue;
            }
            boolean isNeighbourLinked = checkLink(zoneState, i, endId, depth,allowedIds);
            linkMatrix[i - 1][endId - 1] = isNeighbourLinked;
            if (isNeighbourLinked) {
                return true;
            }
        }
        return false;
    }
}
