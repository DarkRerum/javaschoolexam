package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.lang.Math.addExact;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int rows = getPyramidHeight(inputNumbers.size());

        if (rows == 0) {
            throw new CannotBuildPyramidException();
        }
        int columns = rows * 2 - 1;

        List<Integer> sortedNumbers = new ArrayList<Integer>();
        sortedNumbers.addAll(inputNumbers);
        sortedNumbers.sort(Comparator.comparingInt(Integer::intValue));

        int [][] triangle = new int[rows][columns];
        int index = 0;

        for (int r = 0; r < rows; r++) {
            int startPosition = columns / 2 - r;
            int endPosition = startPosition + r * 2;
            for (int c = 0; c < columns; c++) {
                if (c >= startPosition && c <= endPosition && (c - startPosition) % 2 == 0) {
                    triangle[r][c] = sortedNumbers.get(index++);
                }
            }
        }

        return triangle;
    }

    /**
     * Checks if given number N is triangular and returns pyramid height (amount of rows needed to build
     * the pyramid with N numbers).
     *
     * @param inputNumber to be used in the pyramid
     * @return height or 0 if inputNumber is not a triangular number
     */
    private int getPyramidHeight(int inputNumber) {
        int currentNumber = 1;
        int delta = 1;

        while (inputNumber >= currentNumber) {
            delta++;
            try {
                currentNumber = addExact(currentNumber, delta);
            }
            catch (ArithmeticException e) {
                return 0;
            }

            if (inputNumber == currentNumber) {
                return delta;
            }
        }

        return 0;
    }
}
